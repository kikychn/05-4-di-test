package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeMockControllerTest_2 {

    @MockBean
    GameLevelService gameLevelServiceMock;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }


//    @TestConfiguration
//    static class config {
//        @Bean
//        @Primary
//        public GameLevelService mockGameLevelService() throws IOException {
//            GameLevelService gameLevelServiceMock = mock(GameLevelService.class);
//            when(gameLevelServiceMock.renderMaze(10, 10, "color")).thenReturn(new byte[]{1, 2, 3});
//            return gameLevelServiceMock;
//        }
//    }

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_correct_body_when_get_maze() throws Exception {
        byte[] expected = {1, 2, 3};
        when(gameLevelServiceMock.renderMaze(10, 10, "color")).thenReturn(expected);

        mockMvc.perform(get("/buffered-mazes/color")
                .param("type", "color"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_PNG))
                .andExpect(content().bytes(expected));

        verify(gameLevelServiceMock).renderMaze(10, 10, "color");
    }

}
