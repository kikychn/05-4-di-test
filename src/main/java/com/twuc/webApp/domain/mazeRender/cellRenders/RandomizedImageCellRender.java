package com.twuc.webApp.domain.mazeRender.cellRenders;

import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.RenderCell;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;


/**
 * 该渲染器使用随机的图片对迷宫网格进行渲染。
 */
public class RandomizedImageCellRender implements CellRender {
    private final List<BufferedImage> textures;
    private final Random random = new Random();

    /**
     * 创建一个 {@link RandomizedImageCellRender} 实例。
     *
     * @param textures 备选的渲染图片列表。
     */
    public RandomizedImageCellRender(List<BufferedImage> textures) {
        this.textures = textures;
    }

    @Override
    public void render(Graphics context, Rectangle cellArea, RenderCell cell) {
        BufferedImage texture = textures.get(random.nextInt(textures.size()));
        int x = ((cellArea.x * 2 + cellArea.width) / 2) - texture.getWidth() / 2;
        int y = (cellArea.y + cellArea.height) - texture.getHeight();
        context.drawImage(texture, x, y, null);
    }
}
