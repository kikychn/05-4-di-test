package com.twuc.webApp.web;

import com.twuc.webApp.domain.mazeGenerator.AldousBroderMazeAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.DijkstraSolvingAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.MazeWriter;
import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GameLevelServiceTest {

    private GameLevelService gameLevelService;

    @Mock
    private MazeWriter mazeWriter;
    @Mock
    private AldousBroderMazeAlgorithm aldousBroderMazeAlgorithm;
    @Mock
    private DijkstraSolvingAlgorithm dijkstraSolvingAlgorithm;

    @BeforeEach
    void setUp() {
        initMocks(this);
        List<MazeWriter> mazeWriters = new ArrayList<>();
        mazeWriters.add(mazeWriter);
        gameLevelService = new GameLevelService(mazeWriters, aldousBroderMazeAlgorithm, dijkstraSolvingAlgorithm);
    }

    @Test
    void should_return_correct_img() throws IOException {
        when(mazeWriter.getName()).thenReturn("color");
        doAnswer((Answer) invocation -> {
            OutputStream outStream = invocation.getArgument(1);
            outStream.write(new byte[]{1, 2, 3});
            return null;
        }).when(mazeWriter).render(any(Grid.class), any(OutputStream.class));


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        gameLevelService.renderMaze(outputStream, 10, 10, "color");
        byte[] bytes = outputStream.toByteArray();
        assertArrayEquals(new byte[]{1, 2, 3}, bytes);
    }
}
