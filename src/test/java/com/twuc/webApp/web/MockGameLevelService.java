package com.twuc.webApp.web;

import com.twuc.webApp.domain.mazeGenerator.AldousBroderMazeAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.DijkstraSolvingAlgorithm;
import com.twuc.webApp.domain.mazeRender.MazeWriter;
import com.twuc.webApp.service.GameLevelService;

import java.io.IOException;
import java.util.List;

public class MockGameLevelService extends GameLevelService {
    public MockGameLevelService() {
        super(null, null, null);
    }

    @Override
    public byte[] renderMaze(int width, int height, String type) throws IOException {
        return new byte[]{1, 2, 3};
    }
}
