package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class MazeControllerTest_1 {
    MazeController mazeController;

    @Mock
    GameLevelService gameLevelServiceMock;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mazeController = new MazeController(gameLevelServiceMock);
    }

    @Test
    void should_return_right_body_when_get_maze() throws IOException {
        //given
        byte[] expected = new byte[]{1, 2, 3};
        when(gameLevelServiceMock.renderMaze(10, 10, "color")).thenReturn(expected);
        //when
        ResponseEntity<byte[]> responseEntity = mazeController.getMaze(10, 10, "color");
        //then
        assertArrayEquals(expected, responseEntity.getBody());
    }

    @Test
    void should_invoke_correct_when_get_maze2() throws IOException {
        MockHttpServletResponse response = new MockHttpServletResponse();
//        mazeController.getMaze2(response, 10, 10, "color");

//        assertEquals(MediaType.IMAGE_PNG_VALUE,response.getContentType());
//        verify(gameLevelServiceMock).renderMaze(response.getOutputStream(),10,10,"color");

        doAnswer((Answer) invocation -> {
            Object arg0 = invocation.getArgument(0);
            Object arg1 = invocation.getArgument(1);
            Object arg2 = invocation.getArgument(2);
            Object arg3 = invocation.getArgument(3);

            assertEquals(response.getOutputStream(), arg0);
            assertEquals(10, arg1);
            assertEquals(10, arg2);
            assertEquals("color", arg3);
            assertEquals(MediaType.IMAGE_PNG_VALUE,response.getContentType());
            return null;
        }).when(gameLevelServiceMock).renderMaze(any(OutputStream.class),anyInt(),anyInt(),anyString());
        mazeController.getMaze2(response, 10, 10, "color");
    }

}